package cz.anitra;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import cz.anitra.model.DeviceList;

public class DevicesFragment extends Fragment {

    ListView lvDeviceList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflateView = inflater.inflate(R.layout.activity_devices, container, false);

        // Find and fill the list of devices
        lvDeviceList = inflateView.findViewById(R.id.deviceList);
        createList();

        // Inflate the layout for this fragment
        return inflateView;
    }

    public void createList() {
        SQLiteDatabase db = NavigationActivity.anitraDbHelper.getReadableDatabase();

        // Query for items from the database and get a cursor back
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = null;
        String selection = "";
        String[] selectionArgs = {};
        // How you want the results sorted in the resulting Cursor
        String sortOrder = DeviceList.Device.COLUMN_NAME_ID + " DESC";

        Cursor cursor = db.query(
                DeviceList.Device.TABLE_NAME,   // The table to query
                null,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        // Setup cursor adapter using cursor from last step
        DeviceCursorAdapter adapter = new DeviceCursorAdapter(getActivity(), cursor);


        // Attach cursor adapter to the ListView
        lvDeviceList.setAdapter(adapter);

        lvDeviceList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3)
            {
                Cursor cur = (Cursor) adapter.getItemAtPosition(position);
                cur.moveToPosition(position);
             /*   Integer id = cur.getInt(cur.getColumnIndexOrThrow("_id"));
                String name = cur.getString(cur.getColumnIndexOrThrow("name"));

                Intent intent = new Intent(getActivity(), BLABLA.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", id);
                bundle.putString("name", name);
                intent.putExtras(bundle);

                startActivity(intent);*/
            }
        });
        //cursor.close();
    }
}