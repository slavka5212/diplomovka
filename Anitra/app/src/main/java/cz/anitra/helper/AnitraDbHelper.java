package cz.anitra.helper;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

import cz.anitra.HTTP.SendGetRequest;
import cz.anitra.NavigationActivity;
import cz.anitra.config.REST_API;
import cz.anitra.model.DeviceList;
import cz.anitra.model.DeviceList.Device;

public class AnitraDbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "Anitra.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + Device.TABLE_NAME + " (" +
                    Device._ID + " INTEGER PRIMARY KEY," +
                    Device.COLUMN_NAME_ID + " INTEGER UNIQUE," +
                    Device.COLUMN_NAME_NAME + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + Device.TABLE_NAME;

    public AnitraDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void getDeviceList(Activity currentActivity, String apiKey) {
        String result = null;
        SendGetRequest sendGetRequest = new SendGetRequest();
        try {
            result = sendGetRequest.execute(REST_API.TRACKED_OBJECT_LIST.URL, apiKey).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        // For testing
        Toast.makeText(currentActivity.getApplicationContext(), result, Toast.LENGTH_LONG).show();

        if (result != null && !result.contains("error")) {
            try {
                // Get list of devices
                JSONObject jsonObject = new JSONObject(result);
                JSONArray list = jsonObject.getJSONArray("list");

                // Save Devices to DB
                for (int i = 0; i < list.length(); i++) {
                    JSONObject c = list.getJSONObject(i);

                    Integer id = c.getInt(REST_API.TRACKED_OBJECT_LIST.ID);
                    String deviceCode = c.getString(REST_API.TRACKED_OBJECT_LIST.CODE);
                    String deviceName = c.getString(REST_API.TRACKED_OBJECT_LIST.NAME);
                    if (!deviceName.equals("") && !deviceName.equals(null)) {
                        deviceCode += " (" + deviceName + ")";
                    }

                    // Get the data repository in write mode
                    SQLiteDatabase db = this.getWritableDatabase();

                    //this.onUpgrade(db, 1, 2);

                    // Create a new map of values, where column names are the keys
                    ContentValues values = new ContentValues();
                    values.put(DeviceList.Device.COLUMN_NAME_ID, id);
                    values.put(DeviceList.Device.COLUMN_NAME_NAME, deviceCode);

                    // Insert the new row, returning the primary key value of the new row
                    long newRowId = db.insert(DeviceList.Device.TABLE_NAME, null, values);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}