package cz.anitra.config;

public final class REST_API {
    public static final String LOGIN = "https://anitra.cz/demo/api/v1/login";
    public static final String API_KEY = "72fe0bbc50c8a76c48a5ca28b629455d360a9c95eb9f2adc05d127610a8488cd83d512b2dff811ac97d297d8cdbcef0a507b704807418bce15917a9ea754a61d";

    public static class TRACKED_OBJECT_LIST {
        public static final String URL = "https://anitra.cz/demo/api/v1/tracked-object/list";

        // seznam posledních pozic všech uživatelových Tracked Objectu
        public static final String LAST_POSITIONS = "https://anitra.cz/demo/api/v1/tracked-object/last-positions";
        // filtrování Tracked Objectu podle času a druhu  ptaku
        public static final String FILTER = "https://anitra.cz/demo/api/v1/tracked-object/filter/";


        public static final String ID = "TrackedObjectId";
        public static final String CODE = "TrackedObjectCode";
        public static final String NAME = "TrackedObjectName";
        public static final String NOTE = "TrackedObjectNote";
    }
    public static class TRACKED_OBJECT {
        public static final String URL = "https://anitra.cz/demo/api/v1/tracked-object/data/";

        // filtrování dat Tracked Objectu podle času a limitu
        public static final String FILTER_URL = "https://anitra.cz/demo/api/v1/tracked-object/data/{id}?limit={limit}&from={from}&to={to}";

        public static final String TO = "trackedobject";
        public static final String TO_ID = "Id";
        public static final String TO_CODE = "TrackedObjectCode";
        public static final String TO_NAME = "TrackedObjectName";
        public static final String TO_NOTE = "TrackedObjectNote";

        public static final String DATA = "data";
        public static final String DATA_LAT = "lat";
        public static final String DATA_LNG = "lng";
        public static final String DATA_ALT = "alt";
        public static final String DATA_TIME = "time";
    }

    // vrácení detailnich dat o bodu, je potřeba znát DeviceDataBasic ID bodu, vrací stejné informace, jako vrací presentery pro mapu
    public static final String POINT_DETAIL = "https://anitra.cz/demo/api/v1/point/detail/{{id}}";

    // seznam druhu ptaku
    public static final String SPECIES = "https://anitra.cz/demo/api/v1/list/species/";
}
