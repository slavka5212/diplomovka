package cz.anitra;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

import cz.anitra.config.REST_API;
import cz.anitra.helper.CheckNetwork;
import cz.anitra.HTTP.SendPostRequest;

/**
 * A login screen that offers login via email and password.
 */
public class LoginActivity extends AppCompatActivity {

    // Login Messages
    private static final String NO_INTERNET = "No Internet Connection";
    private static final String AUTHENTICATION_ERROR = "Authentication error";
    private static final String AUTHENTICATION_SUCCESS = "Authentication success";

    // UI references.
    private AutoCompleteTextView actvEmail;
    private EditText etPassword;

    // Preferences
    public static SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set shared preferences about user
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);

        // Put API KEY for testing
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.api_key), REST_API.API_KEY);
        editor.apply();

        // Check if user is remembered
        if (!sharedPref.getString(getString(R.string.api_key), "").equals("")) {
            Intent intent = new Intent(LoginActivity.this, NavigationActivity.class);
            startActivity(intent);
        }

        // Set up the login form.
        actvEmail = findViewById(R.id.email);
        etPassword = findViewById(R.id.password);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(CheckNetwork.isInternetAvailable(LoginActivity.this)) {
                    // Login with post request
                    String result = null;
                    HashMap<String, String> postParams = new HashMap<String, String>();

                    postParams.put("password", String.valueOf(etPassword.getText()));
                    postParams.put("email", String.valueOf(actvEmail.getText()));
                    SendPostRequest postRequest = new SendPostRequest(postParams);
                    try {
                        result = postRequest.execute(REST_API.LOGIN).get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    // For testing
                    Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();

                    if (result != null && !result.contains("error")) {
                        // Get API_KEY
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            String api_key = jsonObject.getString(getString(R.string.api_key));
                            // Save API_KEY
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(getString(R.string.api_key), api_key);
                            editor.apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(LoginActivity.this, NavigationActivity.class);
                        startActivity(intent);
                    }
                }
                else {
                    Toast.makeText(LoginActivity.this,NO_INTERNET ,Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}

