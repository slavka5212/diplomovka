package cz.anitra.model;

import android.provider.BaseColumns;

public final class DeviceList {

    private  DeviceList() {}

    /* Inner class that defines the table device */
    public static class Device implements BaseColumns {
        public static final String TABLE_NAME = "device";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_NAME = "name";
    }

    /* Inner class that defines the table data */
    public static class DeviceData implements BaseColumns {
        public static final String TABLE_NAME = "data";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_LONG = "lng";
        public static final String COLUMN_NAME_ALT = "alt";
        public static final String COLUMN_NAME_TIME = "time";
    }
}
