package cz.anitra.HTTP;

import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class SendGetRequest extends AsyncTask<String, Void, String> {

    protected void onPreExecute(){
        super.onPreExecute();
    }

    protected String doInBackground(String... params) {

        try {
            String stringUrl = params[0];
            String apiKey = params[1];
            URL url = new URL(stringUrl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.addRequestProperty("Authorization", apiKey);

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader in = new BufferedReader(new
                        InputStreamReader(
                        conn.getInputStream()));

                StringBuilder sb = new StringBuilder("");
                String line = in.readLine();

                while(line != null) {
                    sb.append(line);
                    line = in.readLine();
                }
                in.close();

                return sb.toString();
            }
            else {
                return "false : " + responseCode;
            }
        }
        catch(Exception e){
            return "Exception: " + e.getMessage();
        }

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }

}
